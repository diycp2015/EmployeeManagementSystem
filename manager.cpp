#include "manager.h"

Manager::Manager(int id,string name,int dID){
    this->m_ID = id;
    this->m_Name = name;
    this->m_DeptId = dID;
}
Manager::~Manager(){

}

void Manager::showInfo(){
    cout << "  职工编号：" << this->m_ID
        << "  职工姓名：" << this->m_Name
        << "  岗位：" << this->getDeptName()
        << "  岗位职责：完成老板交代的任务，下发任务给员工" << endl;
}

std::string Manager::getDeptName(){
    return string("经理");
}
