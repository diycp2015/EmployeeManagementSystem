#ifndef _WORKERMANAGER_H_
#define _WORKERMANAGER_H_

#include <iostream>
#include "worker.h"
#include "employee.h"
#include "manager.h"
#include "boss.h"
#include <fstream>
#define FILENAME "empFile.txt"

using namespace std;

class WorkerManger{
public:
    WorkerManger();
    virtual ~WorkerManger();
    void showMenu();//显示菜单
    void exitSystem();//退出系统
    void Add_Employee();//增加职工
    void saveInfo();//保存员工信息
    int get_EmpNum();//初始化员工个数
    void initEmployee();//初始化员工
    void show_Emp();//显示员工
    void delEmployee();//删除员工
    int IsExist(int id);//按照职工编号判断员工是否存在，存在返回在数组中位置，否则返回-1
    void modifyEmployee();//修改员工信息
    void findEmployee();//查找职工
    void sortEmplyee();//按职工编号进行排序
    void clearFile();//清空文件
public:
    int m_EmpNum;//记录文件中的人数
    Worker ** m_EmpArray;//员工数组指针
    bool m_fileIsEmpty;//判断存储员工信息的文件是否为空
};

#endif
