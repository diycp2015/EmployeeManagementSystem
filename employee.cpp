#include "employee.h"

Employee::Employee(int id,string name,int dID){
    this->m_ID = id;
    this->m_Name = name;
    this->m_DeptId = dID;
}
Employee::~Employee(){

}

void Employee::showInfo(){
    cout << "  职工编号：" << this->m_ID
        << "  职工姓名：" << this->m_Name
        << "  岗位：" << this->getDeptName()
        << "  岗位职责：完成经理交给的任务" << endl;
}

std::string Employee::getDeptName(){
    return string("员工");
}
