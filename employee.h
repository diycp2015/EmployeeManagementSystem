#ifndef _EMPLOYEE_H_
#define _EMPLOYEE_H_

#include <iostream>
using namespace std;
#include "worker.h"
//员工类
class Employee :public Worker{
    
public:
    Employee(int id,string name,int dID);
    virtual ~Employee();

    virtual void showInfo();
    virtual string getDeptName();
};

#endif
