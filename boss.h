#ifndef _BOSS_H_
#define _BOSS_H_

#include <iostream>
using namespace std;
#include "worker.h"
#include <string>

//员工类
class Boss :public Worker{
    
public:
    Boss(int id,string name,int dID);
    virtual ~Boss();

    virtual void showInfo();
    virtual std::string getDeptName();
};

#endif
