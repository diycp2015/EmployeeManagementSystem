src = $(wildcard *.cpp)
targets = $(patsubst %.c,%.o,$(src))
#
#all:$(targets)
#
#$(targets):%:%.c
#	g++ $< -o $@

edit:$(targets) *.h
	g++ -o edit $(targets)

#$(objects):*.h

%.o:%.c
	g++ -c $< -o $@

.PHONY:clean
clean:
	-rm  *.o edit
