#ifndef _MANAGER_H_
#define _MANAGER_H_

#include <iostream>
using namespace std;
#include "worker.h"

//员工类
class Manager :public Worker{
    
public:
    Manager(int id,string name,int dID);
    virtual ~Manager();

    virtual void showInfo();
    virtual string getDeptName();
};

#endif
