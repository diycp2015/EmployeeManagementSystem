#include "workerManger.h"

WorkerManger::WorkerManger(){
    this->initEmployee();    
    ifstream ifs;
    ifs.open(FILENAME,ios::in);
    //文件不存在情况
    if(!ifs.is_open()){
        cout << "文件不存在！" << endl;
        this->m_EmpNum = 0;//初始化人数
        this->m_fileIsEmpty = true;//初始化文件为空
        this->m_EmpArray = NULL;//初始化数组
        ifs.close();
        return ;
    }

    //文件存在，但是为空
    char ch;
    ifs >> ch;
    if(ifs.eof()){
        cout << "文件为空！" << endl;
        this->m_EmpNum = 0;//初始化人数
        this->m_fileIsEmpty = true;//初始化文件为空
        this->m_EmpArray = NULL;//初始化数组
        ifs.close();
        return ;
    }
    //文件有记录
    int num = get_EmpNum();
    cout << "员工个数为：" << num << endl;
    this->m_EmpNum = num;
    this->m_fileIsEmpty = false;
    this->m_EmpArray = new Worker*[this->m_EmpNum];
    //初始化员工
    initEmployee();

    for(int i = 0;i < m_EmpNum;i++){
        cout << this->m_EmpArray[i]->m_ID << " "
            << this->m_EmpArray[i]->m_Name << " "
            << this->m_EmpArray[i]->m_DeptId << endl;
    } 

}

WorkerManger::~WorkerManger(){  
    if(this->m_EmpArray != NULL){
        delete[] this->m_EmpArray;
    }
    this->m_EmpArray = NULL;
}

void WorkerManger::showMenu(){
    
    cout << " ************************************** " << endl;
    cout << " *********欢迎使用职工管理系统********* " << endl;
    cout << " ************0.退出管理系统************ " << endl;
    cout << " ************1.增加职工信息************ " << endl;
    cout << " ************2.显示职工信息************ " << endl;
    cout << " ************3.删除离职员工************ " << endl;
    cout << " ************4.修改职工信息************ " << endl;
    cout << " ************5.查找职工信息************ " << endl;
    cout << " ************6.按照编号排序************ " << endl;
    cout << " ************7.清空所有文档************ " << endl;
    cout << " ************************************** " << endl;

}

void WorkerManger::exitSystem(){
    cout << "欢迎下次使用，谢谢！！！" << endl;
    exit(0);
}

void WorkerManger::Add_Employee(){
    
    cout << "请输入添加职工个数：" << endl;
    int addNum = 0;
    cin >> addNum;

    if(addNum > 0){
        //计算新空间大小
        int newSize = this->m_EmpNum + addNum;
        //开辟新空间
        Worker ** newSpace = new Worker*[newSize];
        //将原空间内容放到新空间下
        if(this->m_EmpArray != NULL){
            for(int i = 0;i < this->m_EmpNum;i++){
                newSpace[i] = this->m_EmpArray[i];
            }
        }
        //输入新员工数据
        for(int i = 0;i < addNum;i++){
            int id,dID;
            string name;
           
            cout << "请输入第" << i+1 << "个员工的职工编号：" << endl;
            cin >> id;
            
            cout << "请输入第" << i+1 << "个员工的姓名：" << endl;
            cin >> name;
            
            cout << "请选择该职工的岗位：" << endl;
            cout << "1.普通员工" << endl;
            cout << "2.项目经理" << endl;
            cout << "3.老板" << endl;
            cin >> dID;
            
            Worker * worker = NULL;
            switch(dID){
                case 1:
                    worker = new Employee(id,name,1);
                    break;
                case 2:
                    worker = new Manager(id,name,2);
                    break;
                case 3:
                    worker = new Boss(id,name,3);
                    break;
                defalut:
                    break;
            }
            newSpace[this->m_EmpNum + i] = worker;
        }
        //释放原有空间
        delete[] this->m_EmpArray;
        //更改空间指向
        this->m_EmpArray = newSpace;
        //更新个数
        this->m_EmpNum = newSize;//前面已更新过
        //更新职工信息文件不为空
        this->m_fileIsEmpty = false;

        cout << "已成功添加 " << addNum << " 新员工！" << endl;
        //保存员工信息
        this->saveInfo();

    }else{
        cout << "输入有误！！！" << endl;
    }
}

void WorkerManger::saveInfo(){
    ofstream ofs;
    ofs.open(FILENAME,ios::out);

    for(int i = 0;i < this->m_EmpNum;i++){
        ofs << this->m_EmpArray[i]->m_ID << " "
            << this->m_EmpArray[i]->m_Name << " "
            << this->m_EmpArray[i]->m_DeptId << " "
            << endl;
    }
    ofs.close();
}

int WorkerManger::get_EmpNum(){
    ifstream ifs;
    ifs.open(FILENAME,ios::in);
    
    int id,dID;
    string name;
    //记录文件中员工个数,并且作为下标传进数组
    int num = 0;
    while(ifs >> id && ifs >> name && ifs >> dID){
        num++;
    }
//    while(ifs >> id && ifs >> name && ifs >> dID){
//        //初始化员工
//        Worker *worker = NULL;
//        //根据不同部门dID创建不同对象
//        if(dID == 1){
//            worker = new Employee(id,name,dID);
//        }else if(dID == 2){
//            worker = new Manager(id,name,dID);
//        }else if(dID == 3){
//            worker = new Boss(id,name,dID);
//        }else{
//            cout << "文件错误！！！" << endl;
//        }
//        this->m_EmpArray[index++] = worker;
//    }
    ifs.close();
    return num;
}
void WorkerManger::initEmployee(){
    ifstream ifs;
    ifs.open(FILENAME,ios::in);
    
    int id,dID;
    string name;
    int index  = 0;
    while(ifs >> id && ifs >> name && ifs >> dID){
        //初始化员工
        Worker *worker = NULL;
        //根据不同部门dID创建不同对象
        if(dID == 1){
            worker = new Employee(id,name,dID);
        }else if(dID == 2){
            worker = new Manager(id,name,dID);
        }else if(dID == 3){
            worker = new Boss(id,name,dID);
        }else{
            cout << "文件错误！！！" << endl;
        }
        this->m_EmpArray[index++] = worker;
    }
    ifs.close();    
}

void WorkerManger::show_Emp(){
    if(this->m_fileIsEmpty){
        cout << "文件不存在或者记录为空！" << endl;
    }else{
        for(int i = 0;i < this->m_EmpNum;i++){
            this->m_EmpArray[i]->showInfo();//利用多态调用接口
        }
    }
}

int WorkerManger::IsExist(int id){
    int index = -1;
    for(int i = 0;i < this->m_EmpNum;i++){
        if(this->m_EmpArray[i]->m_ID == id){
            index = i;
            break;
        }
    }
    return index;
}

void WorkerManger::delEmployee(){
    if(this->m_fileIsEmpty){
        cout << "文件不存在或者记录为空！！！" << endl;
    }else{
        //按照职工编号删除
        cout << "请输入想删除的员工编号：" << endl;
        int id = -1;
        cin >> id;
        int index = this->IsExist(id);
        if(index != -1){//员工存在
            for(int i = index; i < this->m_EmpNum;i++){
                this->m_EmpArray[i] = this->m_EmpArray[i+1];
            }
            this->m_EmpNum--;
            this->saveInfo();//删除后数据同步到文件
            cout << "删除成功！！！" << endl;
        }else{
            cout << "删除失败，未找到该员工！！！" << endl;
        }
    }
}

void WorkerManger::modifyEmployee(){
    if(this->m_fileIsEmpty){
        cout << "文件不存在或者文件记录为空！！!" << endl;
    }else{
        cout << "请输入要修改员工的编号：" << endl;
        int id = -1;
        cin >> id;
        int index = this->IsExist(id);
        if(index != -1){
            delete this->m_EmpArray[index];//先删除该员工

            int newId = 0;
            string newName = "";
            int newDid = 0;

            cout << "查到编号为 " << id  << " 的员工，请您输入新的员工编号：" << endl;
            cin >> newId;

            cout << "请输入新的姓名：" << endl;
            cin >> newName;

            cout << "请输入岗位：" << endl;
            cout << "1.普通员工" << endl;
            cout << "2.项目经理" << endl;
            cout << "3.老板" << endl;
            cin >> newDid;
            Worker* worker = NULL; 
            switch(newDid){
                case 1:
                    worker = new Employee(newId,newName,newDid);
                    break;
                case 2:
                    worker = new Manager(newId,newName,newDid);
                    break;
                case 3:
                    worker = new Boss(newId,newName,newDid);
                    break;
                defalut:
                    break;
            }
            this->m_EmpArray[index] = worker;
            cout << "修改成功！！！" << endl;
            this->saveInfo();//重新保存
        }else{
            cout << "修改失败，查无此人！！！" << endl;
        }
    }
}

void WorkerManger::findEmployee(){
    if(this->m_fileIsEmpty){
        cout << "文件不存在或者记录为空！！！" << endl;
    }else{
        cout << "请选择查找方式：" << endl;
        cout << "1.按照职工编号查找 " << endl;
        cout << "2.按照职工姓名查找 " << endl;
        int sel = 0;
        cin >> sel;

        if(sel == 1){
            cout << "请输入查找的职工编号：" << endl;
            int id;
            cin >> id;

            int index = IsExist(id);
            if(index == -1){
                cout << "查找失败，查无此人，请确认输入编号是否正确！！！" << endl;
            }else{
                cout << "查找成功，该职工信息如下：" << endl;
                this->m_EmpArray[index]->showInfo();
                cout << endl;
            }
        }else if(sel == 2){
            string name;
            cout << "请输入您要查找的职工姓名：" << endl;
            cin >> name;
            bool flag = false;//查找到的标志
            for(int i = 0;i < this->m_EmpNum;i++){
                if(this->m_EmpArray[i]->m_Name == name){
                    cout << "查找成功，该职工编号为 " << this->m_EmpArray[i]->m_ID << " 的信息如下: "<< endl;
                    this->m_EmpArray[i]->showInfo();
                    flag = true;
                }
            }
            if(!flag){
                cout << "查找失败，查无此人，请确认您输入的姓名是否有误！！！" << endl;
            }
        }else{
            cout << "输入有误，请您正确输入！！！" << endl;
        }
    }
}

void WorkerManger::sortEmplyee(){
    if(this->m_fileIsEmpty){
        cout << "文件不存在或者记录为空！！！" << endl;
    }else{
        cout << "请您输入排序方式：" << endl;
        cout << "1.按升序排序" << endl;
        cout << "2.按降序排序" << endl;
        int sel = 0;
        cin >> sel;
        
        for(int i = 0;i < m_EmpNum;i++){
            int minOrMax = i;
            for(int j = i+1;j < m_EmpNum;j++){
                if(sel == 1){//升序
                    if(m_EmpArray[minOrMax]->m_ID > m_EmpArray[j]->m_ID){
                        minOrMax = j;
                    }
                }else{
                    if(m_EmpArray[minOrMax]->m_ID < m_EmpArray[j]->m_ID){
                        minOrMax = j;
                    }
                }
            }
            if(i != minOrMax){
                Worker *temp = this->m_EmpArray[i];
                m_EmpArray[i] = m_EmpArray[minOrMax];
                m_EmpArray[minOrMax] = temp;
            }
        }
        cout << "排序成功！！！" << endl;
        this->saveInfo();
        this->show_Emp();
    }
}



void WorkerManger::clearFile(){
    cout << "请您再次确认是否需要清空所有信息：" << endl;
    cout << "1. 确认" << endl;
    cout << "2. 返回" << endl;
    int sel = 0;
    cin >> sel;

    if(sel == 1){
        //打开模式 ios::trunc 如果文件存在删除文件并重新创建
        ofstream ofs(FILENAME,ios::trunc);
        ofs.close();

        if(this->m_EmpArray != NULL){
            for(int i = 0;i < m_EmpNum ;i++){
                if(this->m_EmpArray[i] != NULL){
                    delete this->m_EmpArray[i];
                }
            }
            this->m_EmpNum = 0;
            this->m_fileIsEmpty = true;
            delete[] this->m_EmpArray;
            this->m_EmpArray = NULL;
        }
        cout << "文件已经清空！！！" << endl;
    }
}
