#ifndef _WORKER_H_
#define _WORKER_H_

#include <iostream>
#include <string>
using namespace std;
//职工类:老板，经理，普通员工
//抽象到一个类中
//职工属性：职工编号，姓名，所在部门编号
//行为：岗位职责信息描述，获取岗位信息
class Worker{
public:
    //显示个人信息
    virtual void showInfo() = 0;
    //获取岗位名称
    virtual string getDeptName() = 0;

    int m_ID;//职工编号
    string m_Name;//姓名
    int m_DeptId;//职工所在部门编号
};

#endif
