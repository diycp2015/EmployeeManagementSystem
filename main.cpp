#include <iostream>
using namespace std;
#include "workerManger.h"
#include "worker.h"
#include "employee.h"
#include "manager.h"
#include "boss.h"

//void test(){
//    Worker *worker = NULL;
//    worker = new Employee(1,"张三",1);
//    worker->showInfo();
//    delete worker;
//
//    worker = new Manager(2,"李四",2);
//    worker->showInfo();
//    delete worker;
//
//    worker = new Boss(3,"唐僧",3);
//    worker->showInfo();
//    delete worker;
//    worker = NULL;
//}

int main(){
    WorkerManger wm;
    int choice = 0;
    while(1){
        wm.showMenu();
        cout << "请输入你需要的操作：" << endl;
        cin >> choice;

        switch (choice){
            case 0:
                wm.exitSystem();
                break;
            case 1:
                wm.Add_Employee();
                break;
            case 2:
                wm.show_Emp();
                break;
            case 3:
                wm.delEmployee();
                break;
            case 4:
                wm.modifyEmployee();
                break;
            case 5:
                wm.findEmployee();
                break;
            case 6:
                wm.sortEmplyee();
                break;
            case 7:
                wm.clearFile();
                break;
            default:
                system("cls");
                break;
        }
    }
    return 0;
}

