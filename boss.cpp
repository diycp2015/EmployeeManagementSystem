#include "boss.h"

Boss::Boss(int id,string name,int dID){
    this->m_ID = id;
    this->m_Name = name;
    this->m_DeptId = dID;
}
Boss::~Boss(){

}

void Boss::showInfo(){
    cout << "  职工编号：" << this->m_ID
        << "  职工姓名：" << this->m_Name
        << "  岗位：" << this->getDeptName()
        << "  岗位职责：管理公司事务" << endl;
}

std::string Boss::getDeptName(){
    return string("BOSS");
}
